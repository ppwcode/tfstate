locals {
  profile = "ppwcode"
}

provider "aws" {
  region  = "${local.region}"
  profile = "${local.profile}"
}
