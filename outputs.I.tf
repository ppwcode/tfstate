output "I-state_bucket" {
  value = {
    id  = "${module.tfstate.I-state_bucket_id}"
    arn = "${module.tfstate.I-state_bucket_arn}"
  }
}

output "I-logging_bucket" {
  value = {
    id  = "${module.tfstate.I-logging_bucket_id}"
    arn = "${module.tfstate.I-logging_bucket_arn}"
  }
}

output "I-lock_table" {
  value = {
    id  = "${module.tfstate.I-lock_table_id}"
    arn = "${module.tfstate.I-lock_table_arn}"
  }
}
